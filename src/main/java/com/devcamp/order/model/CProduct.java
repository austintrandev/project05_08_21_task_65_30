package com.devcamp.order.model;

import javax.persistence.*;

@Entity
@Table(name="product")
public class CProduct {
	public CProduct(long id, String name, String type, String color, long price) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.color = color;
		this.price = price;
	}

	public CProduct() {
		super();
	}
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="type")
	private String type;
	
	@Column(name="color")
	private String color;
	
	@Column(name="price")
	private long price;
	
	@ManyToOne
    @JoinColumn(name="order_id")
	private COrder order;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

}
