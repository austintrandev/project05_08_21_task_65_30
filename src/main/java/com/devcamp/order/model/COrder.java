package com.devcamp.order.model;

import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="orders")
public class COrder {
	public COrder() {
		super();
	}

	public COrder(String orderCode, String pizzaSize, String pizzaType, String voucherCode, long price, long paid) {
		super();
		this.orderCode = orderCode;
		this.pizzaSize = pizzaSize;
		this.pizzaType = pizzaType;
		this.voucherCode = voucherCode;
		this.price = price;
		this.paid = paid;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="order_code", unique = true)
	private String orderCode;
	
	@Column(name="pizza_size")
	private String pizzaSize;
	
	@Column(name="pizza_type")
	private String pizzaType;
	
	@Column(name="voucher_code", unique = true)
	private String voucherCode;
	
	@Column(name="price")
	private long price;
	
	@Column(name="paid")
	private long paid;
	
	@ManyToOne
    @JoinColumn(name="customer_id")
	private CCustomer customer;
	
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    @JsonManagedReference
	private Set<CProduct> product;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getPizzaSize() {
		return pizzaSize;
	}

	public void setPizzaSize(String pizzaSize) {
		this.pizzaSize = pizzaSize;
	}

	public String getPizzaType() {
		return pizzaType;
	}

	public void setPizzaType(String pizzaType) {
		this.pizzaType = pizzaType;
	}

	public String getVoucherCode() {
		return voucherCode;
	}

	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public long getPaid() {
		return paid;
	}

	public void setPaid(long paid) {
		this.paid = paid;
	}

	public Set<CProduct> getProduct() {
		return product;
	}

	public void setProduct(Set<CProduct> product) {
		this.product = product;
	}

}
