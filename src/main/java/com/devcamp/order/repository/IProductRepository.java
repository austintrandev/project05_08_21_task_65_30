package com.devcamp.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.order.model.*;

public interface IProductRepository extends JpaRepository<CProduct, Long> {

}
